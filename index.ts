import {HelloWorld} from "./src/HelloWorld";
import * as express from "express";
import Express = e.Express;
import e = require("express");
import Request = e.Request;
import Response = e.Response;
import * as http from "http";

// Initializing the express application.
let app: Express = express();

// Starting the server.
let server: http.Server = app.listen(8081, serverStarted);

// Setting a get route for home.
app.get("/", helloWorld);

// Called for a get on home.
function helloWorld(req: Request, res: Response): void {
    res.send(new HelloWorld().talk());
}

// Called when the server is started.
function serverStarted(): void {
    let host: string = server.address().address;
    let port: number = server.address().port;

    console.log(`Server started at: ${host}:${port}`);
}