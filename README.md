
# Typescript Node REST API Starter

An example Typescript Node REST API Starter

## Installation

Clone the repository and do:

    npm install

## Building the Application

    npm run build
    
## Running the Application

    npm run start